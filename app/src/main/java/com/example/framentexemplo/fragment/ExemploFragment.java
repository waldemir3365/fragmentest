package com.example.framentexemplo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.framentexemplo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExemploFragment extends Fragment {


    public ExemploFragment() {
        // Required empty public constructor
    }


    private View view ;
    private Button btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_exemplo, container, false);

        btn = view.findViewById(R.id.bt);
        init();
        return view;
    }


    private void init(){
        btn.setOnClickListener(message());
    }

    private View.OnClickListener message() {
        return view->{

            Toast.makeText(getContext(), "clickbotao", Toast.LENGTH_LONG).show();
        };
    }

}
