package com.example.framentexemplo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.framentexemplo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SegundoFragment extends Fragment {


    public SegundoFragment() {
        // Required empty public constructor
    }

    private View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_segundo, container, false);


        return view;
    }

}
