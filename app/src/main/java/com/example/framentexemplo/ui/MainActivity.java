package com.example.framentexemplo.ui;

import android.content.Intent;
import android.graphics.Color;
import android.renderscript.ScriptIntrinsicColorMatrix;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.framentexemplo.R;
import com.example.framentexemplo.fragment.ExemploFragment;
import com.example.framentexemplo.fragment.SegundoFragment;

public class MainActivity extends AppCompatActivity {


    private Button btn_obter;
    private Button btn_next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_obter = findViewById(R.id.btn_obter);
        btn_next = findViewById(R.id.btn_next);
        init();

    }

    private void init() {

        btn_obter.setOnClickListener(obterFragment());
        btn_next.setOnClickListener(trocaFragment());

    }

    private View.OnClickListener trocaFragment() {

        return view->{
            btn_next.setBackgroundColor(getResources().getColor(R.color.black));
            btn_next.setTextColor(getResources().getColor(R.color.white));
            segundoFragment();
        };
    }

    private void segundoFragment() {

        SegundoFragment segundoFragment = new SegundoFragment();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        ft.replace(R.id.fragment_exemplo, segundoFragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        btn_next.setTextColor(getResources().getColor(R.color.black));
        btn_next.setBackgroundColor(Color.LTGRAY);
    }

    private View.OnClickListener obterFragment() {

        return view->{

            createFragment();

        };
    }

    private void createFragment() {
        ExemploFragment fragment = new ExemploFragment();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.add(R.id.fragment_exemplo,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
